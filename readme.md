# Sprint 1
[![NPM](https://img.shields.io/npm/l/react)](https://github.com/devsuperior/sds1-wmazoni/blob/master/LICENSE)

# Sobre a Sprint 1
Durante a Sprint, o foco será abordar temas abrangentes relacionados a organização, desenvolvimento ágil, testes e cybersecurity. O uso do Git e Gitlab para interpretação de código e objetos de aprendizado é essencial. Será necessário criar um README seguindo as orientações fornecidas, realizar a interpretação dos exercícios propostos no projeto e apresentar o trabalho no final da Sprint 1. As apresentações serão individuais. Recomenda-se a utilização da ferramenta Miro e a criação de quadros Kanban para melhor gerenciamento das tarefas ao longo do programa de bolsas. A colaboração com colegas deve ser mencionada no README ou diretamente no código/texto criado.

## Dia 1: Onboard
No dia 1 aprendemos a fazer o uso do Git e GitLab para fazer o uso correto do README com vídeos explicativos sobre estudos e projetos de automação quando for o caso.

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/d48b2f27-952c-48f3-9094-969690f9551a" width="200px" />
</div>
<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/d4cf2d2e-97d2-473a-b790-828e665583b8" width="190px" />
</div>

Peguei referência do site Java Full Stack

https://git-scm.com/downloads/logos	- Imagem 1

https://www.edivaldobrito.com.br/wp-content/uploads/2023/09/gitlab-lancou-atualizacoes-de-seguranca-para-uma-falha-critica.webp	 - Imagem 2

## Dia 2: Planning Sprint 1
No dia 2 aprendemos a definição de Scrum, suas teorias, valores, eventos, artifacts, backlogs e incrementos.
<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/f3a38c55-afc8-4fba-bfc6-0c586247190c" width="200px" />
</div>
Aprendemos também sobre a importância da comunicação em projetos, o que pode ocorrer com falhas de comunicação e formas de otimizar a comunicação na gestão de projetos.
<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/4e7309d0-0a4a-4565-969e-768535ae37c0" width="300px" />
</div> 

O Sprint Planning é um evento que inicia um novo ciclo de execução, envolvendo a priorização de atividades, definição de metas e colaboração entre Product Owner, Scrum Master e equipe. 

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/238c16b6-97bd-4e91-b1ad-3836eb8dccb9" width="500px" />
</div>

Os 12 Princípios do Manifesto Ágil

   <div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/77f59663-c82a-4b60-b822-0f410ad4ce40" width="400px" />
</div>


Os 4 valores do Manifesto Ágil:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/0156317a-215f-487e-8b0d-a76278547daa" width="350px" />
</div>
https://blog.geekhunter.com.br/manifesto-agil/ - Pesquisa

https://www.ieepeducacao.com.br/sprint-planning/ - Pesquisa

https://static-00.iconduck.com/assets.00/scrum-icon-2048x1586-y3fa1kw2.png - Imagem 1

https://storage.googleapis.com/dpw/app/uploads/2008/11/web-sites-e-comunicacao-21-maneiras-de-os-sites-se-comunicarem-com-seus-visitantes.png - Imagem 2

https://scrumorg-website-prod.s3.amazonaws.com/drupal/s3fs-public/styles/cke_media_resize_medium/public/2023-11/Sprint-Planning-v2.jpg?itok=sIFIEfjr - Imagem 3

https://manifestoagil.com.br/wp-content/uploads/2021/07/principios-manifesto-agil.jpg - Imagem 4

https://s3.amazonaws.com/bucket.sankhya/sankhya/wp-content/uploads/2022/12/02120116/Valores-do-manifesto-agil.png - Imagem 5

## Dia 3: Scrum, Papéis e Responsabilidades

Papéis e Responsabilidades dos Desenvolvedores:

 <div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/91fc25c3-d588-46b2-a87d-ec3c87d9fb88" width="400px" />
</div>

-Estimar os itens do backlog do produto;

-Gerenciar/Atualizar o backlog da Sprint;

Papéis dentro de uma equipe ágil

Product Owner

 <div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/20a07bb9-bcb8-46bc-a045-328b8c27df6a" width="400px" />
</div>

Scrum Master

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/68a451a6-a469-4fc4-a93e-fc2e91dda46d" width="400px" />
</div>

Introdução ao Framework Scrum

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/07e95008-9a54-4be9-975a-c0e954c7a386" width="400px" />
</div>

Pilares do Scrum

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/c352ea36-75b3-4fa9-89b2-f313156ade66" width="400px" />
</div>

Resumo dos processos do Scrum

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/7517f57c-f889-4610-9580-e38c26fc85d4" width="600px" />
</div>


O QA dentro de um time Ágil para que serve?

-Desempenhar seu trabalho de uma maneira que minimize custos, tempo e efeitos indesejáveis;

-Ajudar os clientes a melhorar seus processos também é válido; 

-Encontrar erros importantes o quanto antes, agregar valor ao negócio, ser aliada do cliente e englobar as missões anteriores.

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/8e015390-3893-4a1f-b422-173e5e1f127d" width="400px" />
</div>
https://www.youtube.com/watch?v=yFUv4HK-leY&t=124s - Pesquisa 1

https://awari.com.br/scrum-conheca-os-principais-papeis-e-responsabilidades/?utm_source=blog&utm_campaign=projeto+blog&utm_medium=Scrum:%20Conhe%C3%A7a%20os%20principais%20pap%C3%A9is%20e%20responsabilidades - Pesquisa 2

https://odonodoproduto.com/epic-feature-and-story-epico-funcionalidade-e-historia/ - Pesquisa 3

https://madeinweb.com.br/wp-content/uploads/2019/01/quais-sao-os-profissionais-envolvidos-no-desenvolvimento-de-aplicativos-5-1280x720.png - Imagem 1

https://agileschool.com.br/wp-content/uploads/2020/03/artigo-po-2003-1.png - Imagem 2

https://media.licdn.com/dms/image/D4D12AQHpS3CafjRUrw/article-cover_image-shrink_720_1280/0/1681218924097?e=2147483647&v=beta&t=i-mK4Ia_0VFDpXA1n0JFbLUPO-QEiLmmVgQ-gPTeKjA - Imagem 3

https://scrumorg-website-prod.s3.amazonaws.com/drupal/inline-images/2023-09/scrum-framework-with-sdo-logo-9.29.23.png - Imagem 4

https://www.scrum.as/academy/5/gfx/scrum_3.3_1.png - Imagem 5

https://realizehub.com/wp-content/uploads/2022/09/blogprocesso-scrum.jpg - Imagem 6

https://www.transperfect.com/sites/default/files/styles/responsive_image_2000/public/media/image/Learning%20About%20Quality%20The%20Difference%20Between%20QA%20%26%20QC.jpg?itok=y3_PRKlh - Imagem 7

## Dia 4: Fundamentos do teste de software
### Grupo 2: Édina, Beatriz, Luana e João.

Os fundamentos do teste de software são cruciais para garantir a qualidade dos projetos de software.

-Entendimento dos requisitos;

-Planejamento de testes;

-Automatização de testes;

-Design de casos de teste;

-Execução de teste;

-Rastreamento de defeitos;

-Colaboração entre desenvolvimento e testes;

-Teste e regressão; 

-Melhoria contínua e Documentação.

## Dia 5: Fundamentos do teste de software (Back-End)

Agrega valor ao projeto porque pode-se antecipar testes e consegue o quanto antes achar bugs e erros para que isso não vá para a produção e não chegue ao cliente.

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/a44d3860-be63-4164-8eb5-8bbf3840c3ec" width="400px" />
</div>


https://miro.medium.com/v2/resize:fit:1076/1*b-rKUD8N_MwymHezBivFdQ.png -Imagem

## Dia 6: Myers e o princípio de Pareto

O Princípio de Pareto, ou regra 80/20, é uma tendência que prevê que 80% dos efeitos surgem a partir de apenas 20% das causas, podendo ser aplicado em várias outras relações de causa e efeito.

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/7ab3757e-b39b-436a-8779-9c5d5b2a8145" width="400px" />
</div>

https://rockcontent.com/br/blog/principio-de-pareto/        -Pesquisa

https://avio.com.br/wp-content/uploads/2021/02/principio-de-pareto-80-20.png        -Imagem

## Dia 7: SQL

### Teste 1:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/50dd9cf3-05c2-43f4-86a1-5b43aa308344" width="500px" />
</div>

### Teste 2:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/6f4cb96e-770e-47f1-930d-ba4a1af9ba9e" width="500px" />
</div>

### Teste 3:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/33781f9f-982a-4252-bd83-5c1670934c9a" width="500px" />
</div>

### Teste 4:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/5a720076-f2d5-41f9-9b24-de5ab8a9a2aa" width="500px" />
</div>

### Teste 5:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/04cc3e5f-1bce-4fd1-8960-c1f611eef07d" width="500px" />
</div>

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/3862a41a-0e16-47be-8a00-9ce67cff28e1" width="500px" />
</div>

E depois de realizar os testes da primeira parte eu comecei a segunda parte:

### Teste 1:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/13fcfc44-5f14-441b-ae7c-dff7d8a29f2d" width="500px" />
</div>

### Teste 2:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/1ef0bafd-339b-497f-a954-d6149b55aa7f" width="500px" />
</div>

### Teste 3:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/87c6212b-ed7f-4e9f-b902-3ef4fd7313f1" width="500px" />
</div>

### Teste 4:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/dfd8d61d-9631-4d65-a910-fc807bf42067" width="500px" />
</div>

### Teste 5:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/c772d6b3-09b6-4aff-8999-b09a61304f02" width="500px" />
</div>

### Teste 6:

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/81f0d5d5-31e2-4184-91d7-04262e0e17e4" width="500px" />
</div>

https://www.youtube.com/watch?v=o8ATzYcVp08&list=PL1lueKDtZ3DcmhZX7pDk4sSgApuZwgVwb -Pesquisa 1

https://sqliteonline.com/ -Pesquisa 2

## Dia 8: NOSQL

1) Realizar uma consulta que conte o número de registros existentes.

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/4fe1e3ca-8cf9-4e25-a4a8-c11863e4be22" width="700px" />
</div>

2) Realizar uma consulta para alterar o usuário com o nome "Teste Start" para "Teste Finish".

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/51bff01c-9f9f-4307-8019-9875f56eae37" width="700px" />
</div>

3) Realizar uma consulta para encontrar o usuário com o nome "Bruce Wayne".

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/79c783de-5f8c-4e8a-bfa2-fe13eea4e1bd" width="700px" />
</div>

4) Realizar uma consulta para encontrar o usuário com o e-mail "ghost_silva@fantasma.com".

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/85605959-aaff-4afb-acf2-991c8b46613d" width="700px" />
</div>

5) Realizar uma consulta para deletar o usuário com e-mail "peterparker@marvel.com".

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/a2efcdd3-fced-489a-8b2e-30bdf3ff0efb" width="700px" />
</div>

1) Realizar uma consulta que apresente produtos com descrição vazia;

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/ae353c5f-eb6e-472c-b932-7e4cce9e771c" width="700px" />
</div>

2) Realizar uma consulta que apresente produtos com a categoria "games";

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/af96fe2b-2d0c-409c-8574-eff25b471263" width="700px" />
</div>

3) Realizar uma consulta que apresente produtos com preço "0";

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/49713f41-1ae7-4d78-ae23-bbcb47e4fb92" width="700px" />
</div>

4) Realizar uma consulta que apresente produtos com o preço maior que "100.00";

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/829c6750-fa5d-40cd-bd14-4385c8c6c331" width="700px" />
</div>

5) Realizar uma consulta que apresente produtos com o preço entre "1000.00" e "2000.00";

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/b4bf192b-7cfe-4c8e-a42a-ce7e0eb4c615" width="700px" />
</div>

6) Realizar uma consulta que apresente produtos em que o nome contenha a palavra "jogo".

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/a14c9090-41e8-47be-8dca-a758c34ba333" width="700px" />
</div>

https://chat.openai.com/ - Pesquisa 1

https://www.youtube.com/watch?time_continue=1790&v=x9tC0eK0GtA&embeds_referring_euri=https%3A%2F%2Fcompasso.sharepoint.com%2F&source_ve_path=MjM4NTE&feature=emb_title - Pesquisa 2

https://www.mongodb.com/community/forums/ - Pesquisa 3

## Dia 9: CyberSecurity

### Vídeo 1: Como ser hackeado e perder todas suas informações;

### Vídeo 2: Novo OWASP TOP 10 - 2021;

### O que é OWASP?

-É uma organização que trabalha com objetivo de aprimorar a segurança de software.

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/40ec77bc-583e-4dab-8ba8-6025725e71be" width="400px" />
</div>


### Vídeo 3: Segurança de rede Wifi Doméstica;

Quais são as ameaças e quais riscos eles podem trazer?

Sequestro de DNS

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/7c91cec3-133f-41d1-9a63-d7c40878ef59" width="100px" />
</div>

Botnets e Proxy

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/c173b32f-8b06-49bf-a12b-97b031567526" width="100px" />
</div>

Monitoramento de tráfego

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/50f8ab9c-b6f5-444a-9539-51a29d6996c3" width="100px" />
</div>

Vazamento de Dados pessoais e senhas

<div align="left">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/8f8e6c90-2a81-4444-a914-2cb046ec2f45" width="100px" />
</div>

### Vídeo 4: Segurança Digital - Dicas para se proteger no dia-a-dia;

### Dica 1: Boas senhas

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/b1ee22a8-cda8-4a97-9922-276af00e488a" width="200px" />
</div>

### Dica 2: Usar um gerenciador de senhas

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/b525e890-5ee1-483e-bc96-01e94a8baaa0" width="400px" />
</div>

### Dica 3: MFA all the things (autenticação multifator)

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/5b471001-fe9d-49f1-9c76-11c5e3c39cc8" width="600px" />
</div>

### Dica 4: Cuidados com Phishing

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/3f089939-66cb-4c34-bdcf-f35105d3bd57" width="400px" />
</div>

### Dica 5: Antivírus

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/fb88adaf-b795-464c-9c71-287635af44e6" width="400px" />
</div>

### Dica 6: Transações na Internet

<div align="center">
<img src="https://github.com/RauberDev/Sprint-1/assets/123962523/31abe364-7279-4f9b-8107-44909dc0b16a" width="400px" />
</div>

Todo conteúdo usado foi achado nos vídeos recomendados pelo site do Estágio.

Imagens usadas:

https://previews.123rf.com/images/noono6/noono62002/noono6200200001/140943549-conceito-de-senha-f%C3%A1cil-senha-1234-escrita-em-um-papel-com-marcador.jpg

https://www.indusface.com/wp-content/uploads/2022/05/KC-Image.png

https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Hacker_behind_PC.svg/1756px-Hacker_behind_PC.svg.png

https://www.google.com/url?sa=i&url=https%3A%2F%2Femojiterra.com%2Fpt%2Frobo%2F&psig=AOvVaw0xlu-8Q8Eb0vk_p9TBDtr_&ust=1709324569087000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCPC55K-w0YQDFQAAAAAdAAAAABAE

https://images.ctfassets.net/63bmaubptoky/6jakcQn6hMXferI5H3iow/9f4999e87ddccfd480683e1b90d881e7/monitoramento-rede-BR-Capterra-Header.png

https://cdn-icons-png.flaticon.com/512/193/193220.png

https://new.safernet.org.br/sites/default/files/content_files/Worst%20Passwords%202015.png

https://www.lastpass.com/-/media/e0fa08c5ac674fb5b777e67f103516e8.png?h=1560&w=2160&la=pt&hash=B93B5FDF7C1A90A7E02C652A917F1149

https://www.iperiusbackup.net/wp-content/uploads/2021/03/imagem12.png

https://lh3.googleusercontent.com/proxy/iA1KQZvkZGxSihv_GUQMhJigdl-IvXT_-ZzgJXMlan6zKv5yXZ7nFqbiSLtoqAQuyIGwK4aJicy_DRbTyEWi3AX2QPsCbok2

https://indicca.com.br/wp-content/uploads/2021/06/Importancia_-Antivirus_Indicca-1.jpg

https://blog.validcertificadora.com.br/wp-content/uploads/2018/12/141542-transacoes-online-6-dicas-para-garantir-seguranca-ao-consumidor.jpg


